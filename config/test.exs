import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :poker_planning, PokerPlanningWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "CtOEP/jx2kppJdlLOdkXf6mJ4vrO541qRZXXZASFH0wTTdFmot9YAFSAvIcAWAaN",
  server: false

# In test we don't send emails.
config :poker_planning, PokerPlanning.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
