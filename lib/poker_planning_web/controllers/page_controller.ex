defmodule PokerPlanningWeb.PageController do
  use PokerPlanningWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def submit_form(conn, %{"room_name" => room_name}) do
    room_url = "/" <> room_name

    redirect(conn, to: room_url)
  end
end
