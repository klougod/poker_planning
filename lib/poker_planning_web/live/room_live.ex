defmodule PokerPlanningWeb.RoomLive do
  use PokerPlanningWeb, :live_view
  import Logger

  @impl true
  def render(assigns) do
    Phoenix.View.render(PokerPlanningWeb.PageView, "room.html", assigns)
  end

  @impl true
  def mount(%{"id" => room_name}, _params, socket) do
    topic = "room:" <> room_name

    user = %User{
      id: :rand.uniform(10_000) |> Integer.to_string,
      joined_at: :os.system_time(:seconds),
      admin?: empty_map?(PokerPlanningWeb.Presence.list(topic))
    } |> Map.from_struct

    if connected?(socket) do
      {:ok, _} = PokerPlanningWeb.Presence.track(self(), topic, user.id, user)
      PokerPlanningWeb.Endpoint.subscribe(topic)
    end

    room = %Room{topic: topic, name: room_name} |> Map.from_struct |> add_room_users(PokerPlanningWeb.Presence.list(topic))

    {
      :ok,
      socket
      |> assign(:id, user.id)
      |> assign(:room, room)
      |> assign(:votes, ["1", "2", "3", "5", "8", "13", "21", "34", "55", "?", "-"])
      |> assign(:temporary_assigns, [room: %{}])
    }
  end

  defp empty_map?(map) when %{} == map, do: true
  defp empty_map?(_), do: false

  defp add_room_users(room, joins) do
    Enum.reduce(joins, room, fn {user, %{metas: [meta| _]}}, room ->
      %{room | users: Map.put(room.users, user, meta)}
    end)
  end

  defp remove_room_users(room, leaves) do
    Enum.reduce(leaves, room, fn {user, _}, room ->
      %{room | users: Map.delete(room.users, user)}
    end)
  end

  defp update_user(%{topic: topic, id: id, vote: vote}) do
    %{metas: [user_data]} = PokerPlanningWeb.Presence.get_by_key(topic, id)
    new_user_data = %{user_data | vote: vote}
    PokerPlanningWeb.Presence.update(self(), topic, id, %{new_user_data | voted?: vote != "-"})
  end

  defp update_user(%{topic: topic, id: id, nickname: nickname}) do
    %{metas: [user_data]} = PokerPlanningWeb.Presence.get_by_key(topic, id)
    new_user_data = %{user_data | nickname: nickname}
    PokerPlanningWeb.Presence.update(self(), topic, id, new_user_data)
  end

  defp broadcast(topic, event, params \\ %{}) do
    PokerPlanningWeb.Endpoint.broadcast_from! self(), topic, event, params
  end

  @impl true
  def handle_event("vote", %{"vote" => new_vote}, socket) do
    %{id: id, room: room} = socket.assigns
    %{topic: room.topic, id: id, vote: new_vote} |> update_user()
    new_room = add_room_users(room, PokerPlanningWeb.Presence.list(room.topic))
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_event("submit-form", %{"nickname" => nickname}, socket) do
    %{id: id, room: room} = socket.assigns
    %{topic: room.topic, id: id, nickname: nickname} |> update_user()
    new_room = add_room_users(room, PokerPlanningWeb.Presence.list(room.topic))
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_event("display-votes", _params, socket) do
    %{room: room} = socket.assigns
    new_room = %{room | show_votes?: true}
    broadcast(room.topic, "display-vote")
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_event("reset-votes", _params, socket) do
    %{id: id, room: room} = socket.assigns
    %{topic: room.topic, id: id, vote: "-"} |> update_user()
    new_room = %{room | show_votes?: false} |> add_room_users(PokerPlanningWeb.Presence.list(room.topic))
    broadcast(room.topic, "reset-vote")
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_info(%{event: "display-vote"}, socket) do
    %{room: room} = socket.assigns
    new_room = %{room | show_votes?: true} |> add_room_users(PokerPlanningWeb.Presence.list(room.topic))
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_info(%{event: "reset-vote"}, socket) do
    %{id: id, room: room} = socket.assigns
    %{topic: room.topic, id: id, vote: "-"} |> update_user()
    new_room = %{room | show_votes?: false} |> add_room_users(PokerPlanningWeb.Presence.list(room.topic))
    {:noreply, socket |> assign(:room, new_room)}
  end

  @impl true
  def handle_info(%Phoenix.Socket.Broadcast{event: "presence_diff", payload: %{joins: joins, leaves: leaves}}, socket) do
    %{room: room} = socket.assigns
    new_room = remove_room_users(room, leaves) |> add_room_users(joins)
    {:noreply, socket |> assign(:room, new_room)}
  end
end
