defmodule Room do
  defstruct [:topic, :name, show_votes?: false, users: %{}]
end
