defmodule User do
  defstruct [:nickname, :id, :joined_at, vote: "-", voted?: false, admin?: false]
end
